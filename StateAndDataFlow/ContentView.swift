//
//  ContentView.swift
//  StateAndDataFlow
//
//  Created by Наталья Миронова on 21.07.2023.
//

import SwiftUI

struct ContentView: View {
	
	@StateObject private var timer = TimeCounter()
	@EnvironmentObject private var user: UserManager
	
	var body: some View {
		VStack {
			Text("Hi, \(user.name)")
				.font(.largeTitle)
				.padding(.top, 100)
			Text(timer.counter.formatted())
				.font(.largeTitle)
				.padding(.top, 100)
			Spacer()
			
			ButtonView(action: timer.startTimer, title: timer.buttonTitle, color: .red)
			
			Spacer()
			
			ButtonView(action: buttonLogOutTapped, title: "LogOut", color: .blue)
				.padding(.bottom, 50)
		}
	}
	
	private func buttonLogOutTapped() {
		user.isRegister.toggle()
	}
	
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
			.environmentObject(UserManager())
	}
}
