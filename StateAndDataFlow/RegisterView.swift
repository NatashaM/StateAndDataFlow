//
//  RegisterView.swift
//  StateAndDataFlow
//
//  Created by Наталья Миронова on 24.07.2023.
//

import SwiftUI

struct RegisterView: View {
	@State private var name = ""
	@EnvironmentObject private var user: UserManager
	private var count = 0
	
	var body: some View {
		VStack {
			HStack {
				TextField("Enter your name", text: $name)
					.multilineTextAlignment(.center)
					.overlay(
						Text(name.count.formatted())
							.foregroundColor(name.count > 2 ? .green : .red)
							.padding(.trailing, 16),
						alignment: .trailing
					)
				
			}
			Button(action: registerUser) {
				HStack {
					Image(systemName: "checkmark.circle")
					Text("OK")
				}
				.disabled(name.count < 3)
			}
		}
	}
	
	private func registerUser() {
		if !name.isEmpty {
			user.name = name
			user.isRegister.toggle()
		}
	}
}

struct RegisterView_Previews: PreviewProvider {
	static var previews: some View {
		RegisterView()
	}
}
