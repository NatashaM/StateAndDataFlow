//
//  UserManager.swift
//  StateAndDataFlow
//
//  Created by Наталья Миронова on 24.07.2023.
//

import Foundation

class UserManager: ObservableObject {
	
	@Published var isRegister: Bool {
		didSet {
			UserDefaults.standard.set(isRegister, forKey: "isRegister")
		}
	}
	
	@Published var name: String {
		didSet {
			UserDefaults.standard.set(name, forKey: "name")
		}
	}
	
	init() {
		self.isRegister = (UserDefaults.standard.object(forKey: "isRegister") as? Bool ?? false)
		self.name = UserDefaults.standard.object(forKey: "name") as? String ?? ""
	}
}
