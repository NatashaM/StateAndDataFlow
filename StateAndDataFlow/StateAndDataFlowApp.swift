//
//  StateAndDataFlowApp.swift
//  StateAndDataFlow
//
//  Created by Наталья Миронова on 21.07.2023.
//

import SwiftUI

@main
struct StateAndDataFlowApp: App {
	@StateObject private var userManager = UserManager()
	
	var body: some Scene {
		WindowGroup {
			StarterView()
				.environmentObject(userManager)
		}
	}
}
