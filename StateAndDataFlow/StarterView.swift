//
//  StarterView.swift
//  StateAndDataFlow
//
//  Created by Наталья Миронова on 24.07.2023.
//

import SwiftUI

struct StarterView: View {
	@EnvironmentObject private var user: UserManager
	
	var body: some View {
		Group {
			if user.isRegister {
				ContentView()
			} else {
				RegisterView()
			}
		}
	}
}

struct StarterView_Previews: PreviewProvider {
	static var previews: some View {
		StarterView()
			.environmentObject(UserManager())
	}
}
